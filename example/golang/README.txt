Application "example" reads audio samples from standard input, sends them
to ASR using gRPC and receives recognition.

1. Build application:

export GO111MODULE=on
go build


2. Simple usage is:

cat ../data/sample16kHz.wav | \
        ./example -addr demo.voicelab.pl:7722 \
        -pid PID -pass PASSWORD \
        -sample_rate 16000

where PID and PASSWORD are access parameters.


3. Use option '-c arg' to pass protocol parameters with 
metadata_config.json. Parameters represent straight forward protocol 
metadata variables. Values in json file are substituted by application 
arguments if specified (with exception of "contenttype" - it is always 
set by the application).

cat ../data/sample16kHz.wav | \
        ./example -addr demo.voicelab.pl:7722 \
        -c metadata_config.json \
        -sample_rate 16000


4. Use '-verbose' to follow all updates:

cat ../data/sample16kHz.wav | \
        ./example -addr demo.voicelab.pl:7722 \
        -c metadata_config.json \
        -sample_rate 16000 \
	-verbose


5. Use other audio source. To use simple microphone input:

arecord -r 16000 -f S16_LE | \
        ./example -addr demo.voicelab.pl:7722 \
        -c metadata_config.json \
        -sample_rate 16000 \
        -speech_complete_timeout=2s

Or to recognize for given period of time:

arecord -r 8000 -f S16_LE | \
        ./example -addr demo.voicelab.pl:7722 \
        -c metadata_config.json \
        -sample_rate 8000 & \
	sleep 5 && killall arecord
