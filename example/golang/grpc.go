package main

import (
	"context"
	"errors"
	"fmt"
	"io"
	"log"
	"strconv"
	"strings"
	"time"

	"voicelab.ai/example/pkg/vlviapb"

	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

type output struct {
	recognition string
	err         error
	timeout     vlviapb.TimeoutType
}

func grpcClient(addr string, r io.Reader, c *config, m map[string]string) (string, time.Duration, error) {
	conn, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		return "", 0, err
	}
	defer conn.Close()
	client := vlviapb.NewVLVIAClient(conn)
	m["contenttype"] = c.MimeType
	if c.Pid != "" {
		m["pid"] = c.Pid
	}
	if c.Password != "" {
		m["password"] = c.Password
	}
	if c.ConfName != "" {
		m["conf-name"] = c.ConfName
	}
	if c.NoInputTimeout != 0 {
		m["no-input-timeout"] = strconv.Itoa(int(c.NoInputTimeout / time.Millisecond))
	}
	if c.SpeechCompleteTimeout != 0 {
		m["speech-complete-timeout"] = strconv.Itoa(int(c.SpeechCompleteTimeout / time.Millisecond))
	}
	ctx := metadata.NewOutgoingContext(context.Background(), metadata.New(m))

	stream, err := client.RecognizeStream(ctx)
	if err != nil {
		return "", 0, err
	}

	done := make(chan *output)
	go formatResponse(stream, done, c.Verbose)
	var out *output

	b := make([]byte, 500)
	frames := vlviapb.AudioFrames{}
For:
	for {
		n, err := r.Read(b)
		if err != nil {
			if err == io.EOF {
				break
			}
			return "", 0, err
		}
		frames.Frames = b[:n]
		if err := stream.Send(&frames); err != nil {
			out = <-done
			return "", 0, err
		}
		select {
		default:
		case o := <-done:
			if o.timeout != vlviapb.TimeoutType_NO_TIMEOUT {
				log.Println("timeout:", o.timeout)
			} else {
				out = o
			}
			break For
		}
	}
	if err := stream.CloseSend(); err != nil {
		return "", 0, err
	}
	t := time.Now()
	if out == nil {
		out = <-done
		if out.timeout != vlviapb.TimeoutType_NO_TIMEOUT {
			log.Println("timeout:", out.timeout)
			out = <-done
		}
	}
	return out.recognition, time.Since(t), out.err
}

func formatResponse(stream vlviapb.VLVIA_RecognizeStreamClient, done chan<- *output, verbose bool) {
	var words []string
	timeout := vlviapb.TimeoutType_NO_TIMEOUT
	for {
		update, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Println("recv error: ", err)
			done <- &output{err: err}
			return
		}
		if timeout != vlviapb.TimeoutType_NO_TIMEOUT {
			done <- &output{err: errors.New("expected no updates after timeout")}
			return
		}
		length := len(words) + int(update.Shift) - len(update.Words)
		if length < 0 || length > len(words) {
			err := fmt.Errorf("recv error: length out of range: text=%v, shift=%d, words=%v\n", words, update.Shift, update.Words)
			log.Println("recv error:", err)
			done <- &output{err: err}
		}
		if verbose {
			fmt.Println(update.Shift, update.Words)
		}
		words = append(words[:length], update.Words...)
		timeout = update.GetTimeout()
		if timeout != vlviapb.TimeoutType_NO_TIMEOUT {
			done <- &output{timeout: timeout}
		}
	}
	done <- &output{recognition: strings.Join(words, " "), err: nil}
}
