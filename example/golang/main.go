package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/url"
	"os"
	"strings"
	"time"
)

func main() {
	codecs := map[string]string{
		"LINEAR16": "audio/L16;rate=%d",
		"MULAW":    "audio/basic",
		"ALAW":     "audio/x-alaw-basic",
	}
	var (
		cfgFileName           = flag.String("c", "", "config file name")
		addr                  = flag.String("addr", "", "server address")
		pid                   = flag.String("pid", "", "project ID")
		confName              = flag.String("conf_name", "", "configuration name")
		password              = flag.String("pass", "", "password")
		noInputTimeout        = flag.Duration("no_input_timeout", 0, "no input timeout")
		speechCompleteTimeout = flag.Duration("speech_complete_timeout", 0, "speech complete timeout")
		encoding              = flag.String("samples_encoding", "LINEAR16", "encoding")
		sampleRate            = flag.Int("sample_rate", 0, "sample rate")
		verbose               = flag.Bool("verbose", false, "additional logging")
	)
	flag.Parse()

	var cfg config
	m := make(map[string]string)
	if *cfgFileName != "" {
		jsonFile, err := os.Open(*cfgFileName)
		if err != nil {
			log.Fatalf("error: config file %s: %v", *cfgFileName, err)
		}
		byteValue, _ := ioutil.ReadAll(jsonFile)
		json.Unmarshal(byteValue, &m)
	}
	if *encoding == "LINEAR16" {
		if *sampleRate != 8000 && *sampleRate != 16000 {
			log.Fatal("error: ", "specify sample rate 8000 or 16000")
		}
	} else if *encoding != "MULAW" && *encoding != "ALAW" {
		log.Fatal("error: ", "unknown sample encoding (LINEAR16, MULAW, ALAW)")
	}
	mimeType := codecs[*encoding]
	if strings.Count(mimeType, "%d") == 1 {
		mimeType = fmt.Sprintf(mimeType, *sampleRate)
	}

	_, err := url.Parse(*addr)
	if err != nil {
		log.Fatal("error: ", err)
	}

	cfg = config{Addr: *addr, Pid: *pid, ConfName: *confName, Password: *password,
		NoInputTimeout: *noInputTimeout, SpeechCompleteTimeout: *speechCompleteTimeout, MimeType: mimeType,
		Verbose: *verbose}

	outFile := os.Stdout
	var (
		resp string
		d    time.Duration
	)
	if resp, d, err = grpcClient(strings.TrimPrefix(cfg.Addr, "grpc://"), os.Stdin, &cfg, m); err != nil {
		log.Fatal("error: ", resp, err)
	}
	fmt.Fprintln(outFile, resp)
	fmt.Fprintln(outFile, "#", d)
	return
}

type config struct {
	Addr                  string
	Pid                   string
	ConfName              string
	Password              string
	NoInputTimeout        time.Duration
	SpeechCompleteTimeout time.Duration
	MimeType              string
	Verbose               bool
}
